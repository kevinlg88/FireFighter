﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour {

	[Header("Player Attributes")]
	public float speed;
	public float jumpStrength;

	[Header("Player Dash Attributes")]
	public float dashVelocity;
	public float dashDuration;
	public float dashCountdown;

	[Header("Player Attacks")]
	public float weakPunchDuration;

	//Player States
	private bool isRunning = false;
	private bool isFacingRight = true;
	private bool isTouchingGround = true;
	private bool isFalling = false;
	private bool isJump = false;
	private bool isDoubleJump = false;
	private bool isDash = false;
	private bool isWeakPunch = false;
	private bool canDoubleJump = false;
	private bool canDash = true;


	private Rigidbody2D rb;
	private Animator anim;
	private SpriteRenderer spriteRender;
	// Use this for initialization
	void Awake () {

		rb = this.GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		spriteRender = this.GetComponent<SpriteRenderer> ();
	}

	void OnCollisionEnter2D(Collision2D col)
	{
		if (col.gameObject.CompareTag ("Ground")) 
		{
			isDoubleJump = false;
			isTouchingGround = true;
			isFalling = false;

		} 
	}

	void OnCollisionStay2D(Collision2D col)
	{
		if (col.gameObject.CompareTag ("Ground")) 
		{
			isDoubleJump = false;
			isJump = false;
			isTouchingGround = true;
			isFalling = false;

		} 
	}

	//Verifica se jogador não está mais tocando o chão
	void OnCollisionExit2D(Collision2D col)
	{
		if (col.collider.CompareTag ("Ground")) 
		{
			isTouchingGround = false;
		} 
	}

	
	// Update is called once per frame
	void FixedUpdate () 
	{
		//#### Mechanics ###
		PlayerMoviment ();
		Dashing ();
		//##################


		//### Animations ###
		Animations();

	}

	private void PlayerMoviment()
	{
		//######### Run ##########################
		float movex = Input.GetAxis ("Horizontal");
		rb.velocity = new Vector2 (movex * speed, rb.velocity.y);


		if (movex > 0) 
		{
			isFacingRight = true;
			isRunning = true;
			spriteRender.flipX = false;

		} 
		else if (movex < 0) 
		{
			isFacingRight = false;
			isRunning = true;
			spriteRender.flipX = true;
			
		}
		else if(rb.velocity.x == 0)
		{
			isRunning = false;
		}
		//#######################################


		//############### Jump #########################
		if (Input.GetButtonDown("Jump")) 
		{
			if (isTouchingGround) {
				rb.velocity = Vector2.up * jumpStrength;
				canDoubleJump = true;
			} 
			else if (!isTouchingGround && canDoubleJump) {
				rb.velocity = Vector2.up * jumpStrength;
				canDoubleJump = false;
				isDoubleJump = true;
			}
		}

		if (rb.velocity.y > 0.01)
		{
			isJump = true;
		}

		if (rb.velocity.y < -0.5)
		{
			isDoubleJump = false;
			isJump = false;
			isFalling = true;
		}
		//##############################################

		// ############ Dash ###########################
		if(Input.GetButtonDown("Fire2"))
		{
			if (canDash) 
			{
				isDash = true;
				canDash = false;
				StartCoroutine (Dash ());
			}
		}
		//############################################

		// -------------- Attacks -------------------
		//### WeakPunch ###
		if (Input.GetButtonDown ("Fire1")) {
			StartCoroutine (WeakPunch ());
		}
	}



		
	//Função para verificar estados


	private void Dashing()
	{
		if (isDash) {
			if (isFacingRight) 
			{
				rb.velocity = Vector2.right * dashVelocity;
			} 
			else
			{
				rb.velocity = Vector2.left * dashVelocity;
			}
		}
	}

	IEnumerator Dash()
	{
		rb.gravityScale = 0;
		yield return new WaitForSeconds (dashDuration);
		isDash = false;
		rb.gravityScale = 1;
		yield return new WaitForSeconds (dashCountdown);
		canDash = true;
	}

	IEnumerator WeakPunch()
	{
		isWeakPunch = true;
		yield return new WaitForSeconds (weakPunchDuration);
		isWeakPunch = false;
	}










	//######################################### ANIMATIONS ##################################################
	private void Animations()
	{
		//###### Running #####
		if (isRunning) {
			anim.SetBool ("isRunning", true);
		} else {
			anim.SetBool ("isRunning", false);
		}
		//##### Falling #####
		if (isFalling) {
			anim.SetBool ("isFalling", true);
		} else {
			anim.SetBool ("isFalling", false);
		}
		//##################

		//##### Jump #####
		if (isJump) {
			anim.SetBool ("isJump", true);
		} else {
			anim.SetBool ("isJump", false);
		}
		//#################

		//###### Double Jump #####
		if (isDoubleJump) {
			anim.SetBool ("isDoubleJump", true);
		} else {
			anim.SetBool ("isDoubleJump", false);
		}
		//################

		//##### Aux Jump Variable #####
		if (isTouchingGround) {
			anim.SetBool ("isTouchingGround", true);
		} else {
			anim.SetBool ("isTouchingGround", false);
		}

		//##### Dash #####
		if (isDash) {
			anim.SetBool ("isDash", true);
		} else {
			anim.SetBool ("isDash", false);
		}
		//################

		//--- Attack ----
		//### WeakPunch ###
		if (isWeakPunch) {
			anim.SetBool ("WeakPunch", true);
		} else {
			anim.SetBool ("WeakPunch", false);
		}

	}



}
