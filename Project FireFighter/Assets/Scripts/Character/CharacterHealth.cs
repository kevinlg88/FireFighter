﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHealth : MonoBehaviour {

	public float maxHealth;
	public Slider healthBar;
	private float currentHealth;
	// Use this for initialization
	void Start () {
		currentHealth = maxHealth;


	}
	
	// Update is called once per frame
	void Update () {
		updateHealthBar ();
		if (Input.GetKeyDown (KeyCode.J)) {
			ReceiveDamage (5);
		}
	}

	void ReceiveDamage(float damage)
	{
		currentHealth -= damage;
		if (currentHealth <= 0) {
			Debug.Log ("Dead");
		}
	}

	void updateHealthBar()
	{
		healthBar.value = currentHealth / maxHealth;
	}
}
