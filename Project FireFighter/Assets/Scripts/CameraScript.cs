﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	public Transform character;
	public float offsetX;
	public float offsetY;

	public float minX;
	public float maxX;
	
	// Update is called once per frame
	void FixedUpdate () {

		if (character.position.x > minX && character.position.x < maxX) {
			this.transform.position = new Vector3 (character.position.x + offsetX, this.transform.position.y, this.transform.position.z);
		}

		this.transform.position = new Vector3 (this.transform.position.x, character.position.y + offsetY, this.transform.position.z);

	}
}
