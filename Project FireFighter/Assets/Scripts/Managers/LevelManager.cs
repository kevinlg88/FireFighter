﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	private static LevelManager _instance;
	public static LevelManager Instance
	{
		get
		{
			if (_instance == null)
			{
				GameObject go = new GameObject ("LevelManager");
				go.AddComponent<LevelManager> ();
			}
			return _instance;
		}
	}




	void Awake()
	{
		DontDestroyOnLoad (this.gameObject);
		_instance = this;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
