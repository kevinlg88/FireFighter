﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {
	private static MenuManager _instance;
	public static MenuManager Instance
	{
		get
		{
			if (_instance == null)
			{
				GameObject go = new GameObject ("MenuManager");
				go.AddComponent<MenuManager> ();
			}
			return _instance;
		}
	}


	void Awake()
	{
		DontDestroyOnLoad (this.gameObject);
		_instance = this;
	}



	public void NewGame()
	{
		SceneManager.LoadScene ("Game");
	}
}
